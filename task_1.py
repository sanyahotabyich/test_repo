"""2. Свяжите переменную с любой строкой, состоящей не менее чем из 15
 символов.
Извлеките из строки первый символ
Извлеките из строки последний символ
Извлеките из строки третий с начала
Извлеките из строки третий с конца
Измерьте длину вашей строки
Переверните строку
Извлеките первые восемь символов
"""
str1 = "qwertyuioplkjaz"
print(str1[0])
print(str1[-1])
print(str1[2])
print(str1[-3])
print(len(str1))
str_list = list(str1)
str2 = ''.join(str_list)
print(str2)


"""3. Свяжите переменную с списком который состоит из элементов от 1 до 10
Извлеките из списка элементы стоящие на нечетных позициях
Найдите максимальное число
Найдите минимальное число
Найдите сумму всех чисел заданного списка
Из списка вида [[1, 2, 3], [4, 5, 6], [7, 8, 9, 10]]. 
Напечатайте только первые элементы вложенных списков, те 1 4 7. 
При помощи цикла for.

"""
my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(my_list[::2])
print("max()", max(my_list))
print("min()", min(my_list))
print("sum()", sum(my_list))


test_list = [[1, 2, 3], [4, 5, 6], [7, 8, 9, 10]]

for i in range(len(test_list)):
    print(test_list[i][0])


"""
. Используя встроенные функции реализуйте:
Объединение 2-х кортежей вида: ('a', 'b', 'c', 'd', 'e') и (1, 2, 3, 4, 5)
в словарь. Результат работы программы: {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}
Конвертация элементов списка ['1', '2', '3'] в числа.
Результат работы программы: [1, 2, 3]
"""

my_tuplel1 = ('a', 'b', 'c', 'd', 'e')
my_tuplel2 = (1, 2, 3, 4, 5)

dic = {}
for i in my_tuplel2:
    dic[my_tuplel1[i-1]] = i
print(dic)

list_1 = []
list_2 = ['1', '2', '3']
for i in list_2:
    list_1.append(int(i))

print(list_1)
"""5. Напишите функцию, которая принимает строку и возвращает 
кол-во заглавных и строчных букв.
'The quick Brow Fox' => 
Upper case characters: 3
Lower case сharacters: 12
"""


def my_cnthar(args: str):
    upper_cnt = 0
    lower_cnt = 0
    up_list = "ASDFGHJKLZXCVBNMQWERTYUIOP"
    low_list = "asdfghjklqwertyuiopzxcvbnm"
    for j in list(args):
        if j in up_list:
            upper_cnt += 1
        elif j in low_list:
            lower_cnt += 1
    print(f"Upper case characters:{upper_cnt}\n"
          f"Lower case сharacters:{lower_cnt}")
    return


my_cnthar('The quick Brow Fox')


"""6. Напишите функцию, преобразующую входную строку в выходную как в примерах, 
Пусть s = "abcdef...xyz", тогда вывод будет таким:
f(s, 1) => "a"
f(s, 2) => "aba"
f(s, 3) => "abcba"
f(s, 4) => "abcdcba"
"""


def f(my_s: str, n: int):
    list_str = list(my_s[0:(n - 1)])
    list_str.reverse()
    s2 = ''.join(list_str)

    return print(my_s[0: n] + s2)


s = "abcdef...xyz"

f(s, 1)
f(s, 2)
f(s, 3)
f(s, 4)


"""7. Реализуйте функцию которая умеет работать с файлами 
(читать из заданного файла, записывать в заданный файл).
 Программа считает количество строк, слов и букв в заданном файле 
 и дописывает эту информацию обратно в файл,
  так же выводит эту информацию на экран. 
"""

# file  = open(access_method, w, r, b, a, x, f_name )
# r - default

# f.write


def cnt_str_wrd_char(args: str):
    myfile = open(args)
    cnt_line = 0
    cnt_wrd = 0
    cnt_char = 0
    try:

        for line in myfile:
            cnt_line += 1
            str3 = line.split()
            cnt_wrd += len(str3)
            print(str3)
            for k in str3:
                cnt_char += len(k)
                ...
    finally:
        myfile.close()
    return print(f"cnt_line = {cnt_line},"
                 f" cnt_wrd = {cnt_wrd}, cnt_char = {cnt_char}")


cnt_str_wrd_char(r'c:\Users\a.shpartsenka\exam_py\test_repo\my_file.txt')
# cnt_str_wrd_char('c:/Users/a.shpartsenka/exam_py/test_repo/my_file.txt')
